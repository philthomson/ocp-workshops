# ocp-workshops

## Lab Cluster Info

This is a public repo with configurations and information for OCP Workshops and the lab cluster. 

The lab cluster is running on AWS ap-southeast-1 region which is physically located in Singapore. The lab cluster is consumuing real money and costs will depend how much it's used and how big it grows.

The lab will auto shutdown every day around 5PM PST. The lab needs to be started everyday manually and should be up and running by 7-8am PST. 

## Add A User

The `cluster-config/auth` folder holds yaml files to configure authentication to the cluster.

To add a user to the cluster we'll need to update the `htpass-secret.yaml` file.

This file is a secret that contains the contents of a htpasswd file. I'm running these commands on a linux laptop but you should be able to use htpasswd and base64 commands on Windows as well.

1. To start use the htpasswd command to create a new user/pass in a temporary file. 

`htpasswd -cbB temppassfile user1 MyPassword!`

2. Get the current content of the htpasswd file from the secret in Git and then decoding with base64.

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: htpasswd-hh5vb
  namespace: openshift-config
data:
  htpasswd: cGhpbDokYXByMSQ0NHdNLnJsYSRUM2c0cDN2UHM4NnRZOEd6UFJmSkwv
type: Opaque
```

```
$ echo cGhpbDokYXByMSQ0NHdNLnJsYSRUM2c0cDN2UHM4NnRZOEd6UFJmSkwv | base64 -d
$ phil:$apr1$44wM.rla$T3g4p3vPs86tY8GzPRfJL/
```
3. Now add the current content of the htpasswd file to your temppassfile

```
vim temppassfile
lab-user:$2y$05$P9TNP/E1XWNuqEcm8mKkrOS8Apw.RDnqVKBI8KRagTNpgEBJLUCWy
phil:$apr1$44wM.rla$T3g4p3vPs86tY8GzPRfJL/

```
4. Next encode the temppassfile in base64. Make sure to disable line wrapping with `-w0`.

```
cat temppassfile | base64 -w0
bGFiLXVzZXI6JDJ5JDA1JFA5VE5QL0UxWFdOdXFFY204bUtrck9TOEFwdy5SRG5xVktCSThLUmFnVE5wZ0VCSkxVQ1d5Cg==
```

5. Now you can update the `htpass-secret.yaml` file in git with your updated encoded htpasswd .Create a new branch for your updated file and then create a merge request to merge your branch into main.

6. If you want your user to be a cluster-admin user add your username to the `admin-group.yaml` file in Git

7. Wait a few minutes for the sync process to complete

Some more info is in the OpenShift docs also.

https://docs.openshift.com/container-platform/4.10/authentication/identity_providers/configuring-htpasswd-identity-provider.html
